// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Object.h"
#include "Game/StaticEnumLibrary.h"
#include "StaticStructLibrary.generated.h"

DECLARE_DELEGATE(FItemFunctionDelegate);

USTRUCT(BlueprintType)
struct NEWTHIRDPERSONCODE_API FInventoryStruct
{
	GENERATED_USTRUCT_BODY();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Range)
		FString ItemName;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Range)
		FString ItemDescription;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Range)
		UTexture2D* ItemImage;

	FItemFunctionDelegate UseDelegate;

	EItemUseType UseType;

	EItemType ItemType;

};

USTRUCT(BlueprintType)
struct NEWTHIRDPERSONCODE_API FEquipableStruct
{

	GENERATED_USTRUCT_BODY();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Offset)
	FVector ItemPosOffset;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Offset)
	FVector ItemAngOffset;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Offset)
	FVector ItemScale;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Details)
	FName ItemSocketName;
};

UCLASS()
class NEWTHIRDPERSONCODE_API UStaticStructLibrary : public UObject
{
	GENERATED_BODY()	
};
