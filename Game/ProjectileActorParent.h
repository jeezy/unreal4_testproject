// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "Enemy/EnemyPawnParent.h"
#include "ProjectileActorParent.generated.h"

UCLASS()
class NEWTHIRDPERSONCODE_API AProjectileActorParent : public AActor
{
	GENERATED_BODY()
	
	USphereComponent* SphereComponent;
	UProjectileMovementComponent* ProjectileComponent;

public:	
	// Sets default values for this actor's properties
	AProjectileActorParent();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Target Info")
	float SphereRadius;

	int32 DamageToInflict;

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	void SetHomingTarget(AActor* aTarget);

	UFUNCTION()
	void OnHit(AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);

};
