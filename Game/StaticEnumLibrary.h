// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Object.h"
#include "StaticEnumLibrary.generated.h"

enum class EItemUseType : uint8
{
	Consume		UMETA(DisplayName = "Consume"),
	Equip		UMETA(DisplayName = "Equip"),
	Use		UMETA(DisplayName = "Use")
};

UENUM(BlueprintType)
enum class EItemType : uint8
{
	Burger	UMETA(DisplayName = "Burger"),
	Water	UMETA(DisplayName = "Water"),
	Helmet	UMETA(DisplayName = "Helmet"),
};

UENUM(BlueprintType)
enum class EEquipmentSlot : uint8
{
	Helmet	UMETA(DisplayName = "Helmet"),
	Shoulders	UMETA(DisplayName = "Shoulders"),
	Chest	UMETA(DisplayName = "Chest"),
	Back	UMETA(DisplayName = "Back"),
	LeftHand	UMETA(DisplayName = "Left Hand"),
	RightHand	UMETA(DisplayName = "Right Hand"),
};

UCLASS()
class NEWTHIRDPERSONCODE_API UStaticEnumLibrary : public UObject
{
	GENERATED_BODY()
};
