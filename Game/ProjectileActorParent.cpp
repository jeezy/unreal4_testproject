// Fill out your copyright notice in the Description page of Project Settings.

#include "NewThirdPersonCode.h"
#include "ProjectileActorParent.h"


// Sets default values
AProjectileActorParent::AProjectileActorParent()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	SphereComponent = CreateDefaultSubobject<USphereComponent>(TEXT("RootComponent"));
	SphereComponent->RegisterComponentWithWorld(GetWorld());
	RootComponent = SphereComponent;
	SphereComponent->InitSphereRadius(SphereRadius);
	SphereComponent->SetNotifyRigidBodyCollision(true);
	SphereComponent->SetCollisionProfileName(FName(TEXT("BlockAll")));
	SphereComponent->OnComponentHit.AddDynamic(this, &AProjectileActorParent::OnHit);
}

// Called when the game starts or when spawned
void AProjectileActorParent::BeginPlay()
{
	Super::BeginPlay();
	ProjectileComponent = NewObject<UProjectileMovementComponent>(this);
	ProjectileComponent->RegisterComponent();
	ProjectileComponent->Velocity = FVector(0, 0, 0);
	ProjectileComponent->ProjectileGravityScale = 0;
	ProjectileComponent->bIsHomingProjectile = true;
	ProjectileComponent->HomingAccelerationMagnitude = 1000;
	
}

// Called every frame
void AProjectileActorParent::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );
}

void AProjectileActorParent::SetHomingTarget(AActor* aActor) {
	ProjectileComponent->HomingTargetComponent = aActor->GetRootComponent();
}

void AProjectileActorParent::OnHit(AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit) {
	AEnemyPawnParent* aEnemy = Cast<AEnemyPawnParent>(OtherActor);
	if (aEnemy) {
		//aEnemy->InflictDamage(DamageToInflict);
	}
	this->Destroy();
}