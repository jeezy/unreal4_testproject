// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "GameFramework/Character.h"
#include "UMG/CustomWidgetParent.h"
#include "UMG/UMGTargetPanel.h"
#include "UMG/UMGCastingWidget.h"
#include "UMG/UMGInventoryWidget.h"
#include "UMG/UMGInventoryOptionMenuWidget.h"
#include "UMG/UMGEquipmentWidget.h"
#include "UMG/UMGCharacterBarWidget.h"
#include "Enemy/EnemyPawnParent.h"
#include "Enemy/EnemyCharacterParent.h"
#include "Elements/PulsingDecalActor.h"
#include "Game/ProjectileActorParent.h"
#include "Game/StaticStructLibrary.h"
#include "Game/StaticEnumLibrary.h"
#include "NewThirdPersonCodeCharacter.generated.h"

UCLASS(config=Game)
class ANewThirdPersonCodeCharacter : public ACharacter
{
	GENERATED_BODY()

	/** Camera boom positioning the camera behind the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	/** Follow camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* FollowCamera;

	float BaseWalkSpeed;

	bool FirstPersonToggled;

	float LastArmLength;

	float NextAttackTime;

	float NextSpellTime;

	class APulsingDecalActor* PulsingDecal;

	FTimerHandle PunchEndHandle;

	FTimerHandle ChargeSpellHandle;

	FTimerHandle CastSpellEndHandle;

	FTimerHandle RespawnHandle;

public:

	ANewThirdPersonCodeCharacter();

	virtual void BeginPlay() override;

	virtual void Tick(float deltaSeconds) override;

	UFUNCTION()
	void ConsumeFood(FString itemName);

	UFUNCTION()
	void EquipItem(FString itemName, EItemType itemType);

	UFUNCTION()
	void UseItem(FString itemName, EItemType itemType);

	UFUNCTION()
	void ToggleInventoryPanel();

	UFUNCTION()
	void ToggleEquipmentPanel();

	void SetEquipmentSlot(FString itemName, EEquipmentSlot equipSlot, UStaticMeshComponent* meshComponent);

	void UnsetEquipmentSlot(EEquipmentSlot equipSlot);
	
	FString GetEquipmentSlot(EEquipmentSlot equipSlot);

	bool HasEquipmentInSlot(EEquipmentSlot equipSlot);

	TMap<uint8, FString> EquipmentSlotMap;
	TMap<FString, class UStaticMeshComponent*> EquipmentMeshMap;

	TArray<int32> TheInventory;

	AActor* theTarget;

	bool IsDead;

	void InflictDamage(int32 damage, AActor* theInflictor);

	int32 Health;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Combat")
		int32 MaxHealth;

	void DrainMana(int32 amount);

	int32 Mana;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Combat")
		int32 MaxMana;

	UPROPERTY(EditDefaultsOnly, Category = "Combat")
	TSubclassOf<APulsingDecalActor> TheDecalObject;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Character)
	TSubclassOf<AProjectileActorParent> TheProjectileObject;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Combat")
	FVector2D AttackDamageRange;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Combat")
	float AttackRange;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Combat")
	float AttackCooldown;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Combat")
	float RespawnTime;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Spells")
	FVector2D SpellDamageRange;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Spells")
	float SpellRange;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Spells")
	float SpellCooldown;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Spells")
	float SpellCastTime;

	/** Base turn rate, in deg/sec. Other scaling may affect final turn rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
	float BaseTurnRate;

	/** Base look up/down rate, in deg/sec. Other scaling may affect final rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
	float BaseLookUpRate;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Character)
	float RunSpeedMulti;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Character)
	float MinScrollAmount;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Character)
	float MaxScrollAmount;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Character)
	float ScrollSpeed;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Character)
	bool IsAttacking;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Character)
	bool IsPunching;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Character)
	bool IsChargingSpell;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Character)
	bool IsCastingSpell;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Character)
	TSubclassOf<UCustomWidgetParent> TargetWidgetPanelClass;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Character)
	TSubclassOf<UCustomWidgetParent> CastingWidgetPanelClass;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Character)
	TSubclassOf<UCustomWidgetParent> InventoryWidgetPanelClass;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Character)
	TSubclassOf<UCustomWidgetParent> InventoryOptionPanelClass;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Character)
	TSubclassOf<UCustomWidgetParent> EquipmentWidgetPanelClass;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Character)
	TSubclassOf<UCustomWidgetParent> CharacterBarWidgetPanelClass;

	class UUMGTargetPanel* aTargetPanel;

	class UUMGCastingWidget* aCastingPanel;

	class UUMGInventoryWidget* aInventoryPanel;

	class UUMGInventoryOptionMenuWidget* aInvOptionPanel;

	class UUMGEquipmentWidget* aEquipmentPanel;

	class UUMGCharacterBarWidget* aCharacterBarPanel;

protected:

	void ScrollTowards();

	void ScrollAway();

	void Use();

	void LeftMousePress();
	
	void LeftMouseReleased();

	void RightMousePress();

	void SelectTarget(AActor* aTarget);

	void DeselectTarget();

	AActor* ClickedTarget();

	void StartSprinting();

	void StopSprinting();

	void ToggleView();

	void EndPunching();

	void EndCastingSpell();

	void EndChargingSpell();

	void CancelSpellCharging();

	bool IsFacingTarget();

	void HotKeyOne();

	bool CanCastSpell();

	bool CanMeleeAttack();

	bool HasEnoughMana(int32 amount);

	bool IsHealthDepleted();

	void CommenceRespawn();

	/** Called for forwards/backward input */
	void MoveForward(float Value);

	/** Called for side to side input */
	void MoveRight(float Value);

	/** 
	 * Called via input to turn at a given rate. 
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void TurnAtRate(float Rate);

	/**
	 * Called via input to turn look up/down at a given rate. 
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void LookUpAtRate(float Rate);

	/** Handler for when a touch input begins. */
	void TouchStarted(ETouchIndex::Type FingerIndex, FVector Location);

	/** Handler for when a touch input stops. */
	void TouchStopped(ETouchIndex::Type FingerIndex, FVector Location);

protected:
	// APawn interface
	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;
	// End of APawn interface

public:
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	/** Returns FollowCamera subobject **/
	FORCEINLINE class UCameraComponent* GetFollowCamera() const { return FollowCamera; }
};

