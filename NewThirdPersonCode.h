// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.

#ifndef __NEWTHIRDPERSONCODE_H__
#define __NEWTHIRDPERSONCODE_H__

#include "Runtime/UMG/Public/UMG.h"
#include "Runtime/UMG/Public/UMGStyle.h"
#include "Runtime/UMG/Public/Slate/SObjectWidget.h"
#include "Runtime/UMG/Public/IUMGModule.h"
#include "Runtime/UMG/Public/Blueprint/UserWidget.h"
#include "EngineMinimal.h"

#endif

#pragma once
