// Fill out your copyright notice in the Description page of Project Settings.

#include "NewThirdPersonCode.h"
#include "PulsingDecalActor.h"


// Sets default values
APulsingDecalActor::APulsingDecalActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void APulsingDecalActor::BeginPlay()
{
	Super::BeginPlay();
	TheWorld = GetWorld();
	PulsingDecal = NewObject<UDecalComponent>(this);
	PulsingDecal->RegisterComponent();
	PulsingDecal->AttachTo(RootComponent);
	FQuat theQuat = FQuat::Identity;
	theQuat.Y = 90;
	PulsingDecal->SetWorldRotation(theQuat);
	PulsingDecal->RelativeRotation = PulsingDecal->RelativeRotation.Add(90, 0, 0);
	PulsingDecal->SetRelativeLocation(FVector(0, 0, 0));
}

void APulsingDecalActor::InitializeDecal(FVector position, FVector scale, FString materialPath)
{
	PulsingDecal->SetWorldScale3D(FVector(scale));
	PulsingDecal->SetDecalMaterial(GeneralStaticFunctionLibrary::LoadMaterialFromPath(materialPath));
	SetActorLocation(position);
	BaseScale = scale;
}

void APulsingDecalActor::InitializePulsing(float pulseSpeed, float pulseScale) 
{
	PulseSpeed = pulseSpeed;
	PulseScale = pulseScale;
	DoesPulse = true;
}

void APulsingDecalActor::SetFollowTarget(AActor* aTarget, FVector offset)
{
	FollowTarget = aTarget;
	FollowOffset = offset;
	IsFollowingTarget = true;
}

void APulsingDecalActor::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );
	if (IsFollowingTarget) {
		if (FollowTarget) {
			SetActorLocation(FollowTarget->GetActorLocation() + FollowOffset);
		}
		else {
			this->Destroy();
		}
	}
	if (DoesPulse && TheWorld) {
		float scaleAmount = FMath::Sin(TheWorld->GetTimeSeconds() * PulseSpeed) * PulseScale;
		PulsingDecal->SetWorldScale3D(FVector(BaseScale.X, BaseScale.Y + scaleAmount, BaseScale.Z + scaleAmount));
	}
}

