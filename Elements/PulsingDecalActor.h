// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "GeneralStaticFunctionLibrary.h"
#include "PulsingDecalActor.generated.h"

UCLASS()
class NEWTHIRDPERSONCODE_API APulsingDecalActor : public AActor
{
	GENERATED_BODY()

	bool IsFollowingTarget;

	AActor* FollowTarget;

	FVector FollowOffset;

	bool DoesPulse;

	float PulseSpeed;
	
	float PulseScale;

	FVector BaseScale;

	UDecalComponent* PulsingDecal;

	UWorld* TheWorld;

public:	
	// Sets default values for this actor's properties
	APulsingDecalActor();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;
	
	void InitializeDecal(FVector position, FVector scale, FString materialPath);

	void InitializePulsing(float pulseSpeed, float pulseScale);

	void SetFollowTarget(AActor* aTarget, FVector offset);
};
