// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "PowerUpParent.h"
#include "PowerUpController.generated.h"

UCLASS()
class NEWTHIRDPERSONCODE_API APowerUpController : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	APowerUpController();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	float GenerateX(float DeltaSeconds);

	float currentTime;										// Keep the current alive time.

	float xCord;

	FTransform oldSpawnLocation;
	
	FVector theLocation;

	FRotator theRotation;

	UPROPERTY(EditDefaultsOnly, Category = "Powerups")
	TSubclassOf<APowerupParent> theSpawningObject;			// Holds the blueprints of the object.
};
