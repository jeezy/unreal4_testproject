// Fill out your copyright notice in the Description page of Project Settings.

#include "NewThirdPersonCode.h"
#include "PowerUpController.h"

// Sets default values
APowerUpController::APowerUpController()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	currentTime = 0.f;
	xCord = 0.f;

	theRotation.ZeroRotator;
	theLocation.X = 0.f;
	theLocation.Y = 100.f;
	theLocation.Z = 800.f;

}

// Called when the game starts or when spawned
void APowerUpController::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void APowerUpController::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

	currentTime = currentTime + 1 * DeltaTime;

	if (currentTime >= 2.f)
	{
		xCord = GenerateX(DeltaTime);

		theLocation.X = xCord;

		FActorSpawnParameters spawnParams;

		spawnParams.Owner = this;
		spawnParams.Instigator = Instigator;

		APowerupParent* theNewObject = GetWorld()->SpawnActor<APowerupParent>(theSpawningObject, theLocation, theRotation, spawnParams );

		currentTime = 0.f;
	}

}

float APowerUpController::GenerateX(float DeltaSeconds)
{
	float passBack = 0.f;

	passBack = FMath::RandRange(-300, 300);
	passBack = passBack + DeltaSeconds;

	return passBack;

}

