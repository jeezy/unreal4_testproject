// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "NewThirdPersonCodeCharacter.h"
#include "Game/StaticStructLibrary.h"
#include "Game/StaticEnumLibrary.h"

class NEWTHIRDPERSONCODE_API GeneralStaticFunctionLibrary : public UObject
{
public:
	GeneralStaticFunctionLibrary();
	~GeneralStaticFunctionLibrary();
	static bool Trace(AActor* ActorToIgnore, const FVector& Start, const FVector& End, FHitResult& HitOut, ECollisionChannel CollisionChannel = ECC_Pawn, bool ReturnPhysMat = false);
	static UTexture2D* LoadTextureFromPath(const FString& path);
	static UMaterial* LoadMaterialFromPath(const FString& path);
	static UStaticMesh* LoadStaticMeshFromPath(const FString& path);
	static bool ItemIDExists(int32 itemID);
	static void GenerateEquipableMap();
	static void GenerateItemMap(class ANewThirdPersonCodeCharacter* theCharacter);
	static void CreateInventoryItem(class ANewThirdPersonCodeCharacter* theCharacter, FString itemName, FString itemDescription, FString imagePath, EItemUseType useType, EItemType itemType);
	static void SetEquipableItemData(FString itemName, FVector posOffset, FVector angOffset, FVector scale, FName socketName);
	static void ApplyForce(AActor* theActor, FVector dir, float force);
	static void ToggleRagdoll(USkeletalMeshComponent* theMesh, AActor* theActor);
	static bool IsActorInLineOfSight(AActor* theActor, AActor* otherActor, float distance);
	template <class T>
	static T RandomArrayValue(TArray<T> theArray);
	static FVector GetRandomSpawnPoint(UWorld* theWorld);
	static FVector2D GetMousePos(UWorld* theWorld);
	static EEquipmentSlot ConvertTypeEnum(EItemType itemType);
	static EEquipmentSlot ConvertNameToSlot(FString slotName);
	static int32 FindItemIDByName(FString itemName);
	static TMap<int32, struct FInventoryStruct> ItemMap;
	static TMap<FString, struct FEquipableStruct> EquipmentDataMap;
};
