// Fill out your copyright notice in the Description page of Project Settings.

#include "NewThirdPersonCode.h"
#include "UMGTargetPanel.h"

void UUMGTargetPanel::BeginTargetInfoRefresh()
{
	GetWorld()->GetTimerManager().SetTimer(RefreshTargetInfoHandle, this, &UUMGTargetPanel::RefreshTargetInfo, 0.5, true);
}

void UUMGTargetPanel::RefreshTargetInfo()
{
	if (TargetObject){
		TargetHealthPercent = TargetObject->GetHealthPercentage();
		TargetManaPercent = TargetObject->GetManaPercentage();
	}
}