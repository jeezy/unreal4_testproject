// Fill out your copyright notice in the Description page of Project Settings.

#include "NewThirdPersonCode.h"
#include "UMGCharacterBarWidget.h"

void UUMGCharacterBarWidget::SetupActions(class ANewThirdPersonCodeCharacter* theCharacter) {
	WidgetTree->ForEachWidget([&](UWidget* Widget) {
		UUMGCharacterBarSlotWidget* actionSlotWidget = Cast<UUMGCharacterBarSlotWidget>(Widget);
		if (actionSlotWidget)
		{
			if (actionSlotWidget->SlotName == "Equipment")
				actionSlotWidget->FunctionDelegate.BindUObject(theCharacter, &ANewThirdPersonCodeCharacter::ToggleEquipmentPanel);
			else if (actionSlotWidget->SlotName == "Inventory")
				actionSlotWidget->FunctionDelegate.BindUObject(theCharacter, &ANewThirdPersonCodeCharacter::ToggleInventoryPanel);
		}
	});
}