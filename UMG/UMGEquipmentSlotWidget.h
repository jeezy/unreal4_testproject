// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "UMG/CustomWidgetParent.h"
#include "NewThirdPersonCodeCharacter.h"
#include "GeneralStaticFunctionLibrary.h"
#include "UMGEquipmentSlotWidget.generated.h"

DECLARE_DELEGATE(FFunctionDelegate);

UCLASS()
class NEWTHIRDPERSONCODE_API UUMGEquipmentSlotWidget : public UCustomWidgetParent
{
	GENERATED_BODY()

	public:

	ANewThirdPersonCodeCharacter* OwningPlayer;

	int32 ItemID;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item Info")
		FString SlotName;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item Info")
		UTexture2D* ItemTexture;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Slot Details")
		FLinearColor SlotColor;

	FFunctionDelegate FunctionDelegate;

	virtual FReply NativeOnMouseButtonDown(const FGeometry& InGeometry, const FPointerEvent& InMouseEvent) override;

	virtual void NativeOnMouseEnter(const FGeometry& InGeometry, const FPointerEvent& InMouseEvent) override;

	virtual void NativeOnMouseLeave(const FPointerEvent& InMouseEvent) override;
	
	
};
