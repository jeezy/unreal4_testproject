// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "UMG/CustomWidgetParent.h"
#include "NewThirdPersonCodeCharacter.h"
#include "UMG/UMGCustomButton.h"
#include "UMG/CustomWidgetParent.h"
#include "Game/StaticStructLibrary.h"
#include "Game/StaticEnumLibrary.h"
#include "GeneralStaticFunctionLibrary.h"
#include "Engine.h"
#include "UMGInventoryOptionMenuWidget.generated.h"

/**
 * 
 */
UCLASS()
class NEWTHIRDPERSONCODE_API UUMGInventoryOptionMenuWidget : public UCustomWidgetParent
{
	GENERATED_BODY()
	
	public:

		class ANewThirdPersonCodeCharacter* OwningPlayer;

		int32 ItemSlot;

		int32 ItemID;

		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Button Details")
		FString ActionButtonText;

		void SetupButtonFunctions();

		void SetActionButtonText();

		UFUNCTION()
			void ConsumeItem();

		UFUNCTION()
			void RemoveItem();

		UFUNCTION()
			void Cancel();
	
};
