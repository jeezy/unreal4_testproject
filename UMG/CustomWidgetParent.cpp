// Fill out your copyright notice in the Description page of Project Settings.

#include "NewThirdPersonCode.h"
#include "CustomWidgetParent.h"

FReply UCustomWidgetParent::NativeOnMouseButtonDown(const FGeometry& InGeometry, const FPointerEvent& InMouseEvent) 
{
	UWorld* theWorld = GetWorld();
	if (CanDrag && !IsDragging && theWorld && NextDragTime < theWorld->GetRealTimeSeconds() )
	{
		NextDragTime = theWorld->GetTimeSeconds() + 1;
		TimeUntilMove = GetWorld()->GetTimeSeconds() + 0.5;
		IsDragging = true;
	}
	return Super::NativeOnMouseButtonDown(InGeometry, InMouseEvent);
}

FReply UCustomWidgetParent::NativeOnMouseButtonUp(const FGeometry& InGeometry, const FPointerEvent& InMouseEvent) 
{
	if (CanDrag && IsDragging)
	{
		IsDragging = false;
	}
	return Super::NativeOnMouseButtonUp(InGeometry, InMouseEvent);
}

FReply UCustomWidgetParent::NativeOnMouseMove(const FGeometry& InGeometry, const FPointerEvent& InMouseEvent) 
{
	return Super::NativeOnMouseMove(InGeometry, InMouseEvent);
}

void UCustomWidgetParent::NativeTick(const FGeometry& MyGeometry, float InDeltaTime) 
{
	if (IsDragging && CanDrag)
	{
		UEngine* theEngine = GEngine;
		UWorld* theWorld = GetWorld();
		if (theWorld && theEngine && TimeUntilMove < theWorld->GetRealTimeSeconds()) {
			FVector2D mousePosition = GetMousePos(theWorld);
			FVector2D localSize = MyGeometry.GetDrawSize();
			FVector2D viewportSize = FVector2D(GEngine->GameViewport->Viewport->GetSizeXY());
			mousePosition.X = FMath::Clamp(mousePosition.X - localSize.X*0.4, 0.0, viewportSize.X - localSize.X*0.75);
			mousePosition.Y = FMath::Clamp(mousePosition.Y - localSize.Y*0.4, 0.0, viewportSize.Y - localSize.Y*0.75);
			this->SetPositionInViewport(mousePosition);
		}
	}
	Super::NativeTick(MyGeometry, InDeltaTime);
}

FVector2D UCustomWidgetParent::GetMousePos(UWorld* theWorld)
{
	UEngine* theEngine = GEngine;
	if (theEngine && theWorld)
	{
		ULocalPlayer* LocalPlayer = theEngine->GetLocalPlayerFromControllerId(theWorld, 0);
		if (LocalPlayer)
		{
			FVector2D mousePosition = LocalPlayer->ViewportClient->GetMousePosition();
			return mousePosition;
		}
	}
	return FVector2D(0, 0);
}