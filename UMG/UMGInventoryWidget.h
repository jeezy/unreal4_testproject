// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "Blueprint/UserWidget.h"
#include "Runtime/UMG/Public/UMG.h"
#include "Runtime/UMG/Public/UMGStyle.h"
#include "Runtime/UMG/Public/Slate/SObjectWidget.h"
#include "Runtime/UMG/Public/IUMGModule.h"
#include "Runtime/UMG/Public/Blueprint/UserWidget.h"
#include "GeneralStaticFunctionLibrary.h"
#include "Game/StaticStructLibrary.h"
#include "UMG/CustomWidgetParent.h"
#include "UMG/UMGInventorySlotWidget.h"
#include "NewThirdPersonCodeCharacter.h"
#include "UMGInventoryWidget.generated.h"

UCLASS()
class NEWTHIRDPERSONCODE_API UUMGInventoryWidget : public UCustomWidgetParent
{
	GENERATED_BODY()
	
	public:
		void GenerateInventoryIcons(class ANewThirdPersonCodeCharacter* theCharacter);

		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item Details")
		FString ItemName;

		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item Details")
		FString ItemDescription;
	
};
