// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "UMG/CustomWidgetParent.h"
#include "Engine.h"
#include "UMGCustomButton.generated.h"

DECLARE_DELEGATE(FFunctionDelegate);

UCLASS()
class NEWTHIRDPERSONCODE_API UUMGCustomButton : public UCustomWidgetParent
{
	GENERATED_BODY()


	public:

		FLinearColor DefaultButtonColor;

		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ButtonDetails")
		FLinearColor ButtonColor;

		FFunctionDelegate FunctionDelegate;

		virtual FReply NativeOnMouseButtonDown(const FGeometry& InGeometry, const FPointerEvent& InMouseEvent) override;
		
		virtual void NativeOnMouseEnter(const FGeometry& InGeometry, const FPointerEvent& InMouseEvent) override;

		virtual void NativeOnMouseLeave(const FPointerEvent& InMouseEvent) override;
};
