// Fill out your copyright notice in the Description page of Project Settings.

#include "NewThirdPersonCode.h"
#include "UMGInventorySlotWidget.h"
#include "Engine.h"

FReply UUMGInventorySlotWidget::NativeOnMouseButtonDown(const FGeometry& InGeometry, const FPointerEvent& InMouseEvent) {
	if (GEngine && ItemID != -1) {
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, ItemName);
		if (OwningPlayer->aInvOptionPanel) {
			OwningPlayer->aInvOptionPanel->RemoveFromViewport();
			OwningPlayer->aInvOptionPanel = NULL;
		}
		UCustomWidgetParent* optionWidget = CreateWidget<UCustomWidgetParent>(OwningPlayer->Controller->CastToPlayerController(), OwningPlayer->InventoryOptionPanelClass);
		optionWidget->AddToViewport(0);
		OwningPlayer->aInvOptionPanel = Cast<UUMGInventoryOptionMenuWidget>(optionWidget);
		OwningPlayer->aInvOptionPanel->ItemSlot = ItemSlot;
		OwningPlayer->aInvOptionPanel->ItemID = ItemID;
		OwningPlayer->aInvOptionPanel->OwningPlayer = OwningPlayer;
		OwningPlayer->aInvOptionPanel->SetupButtonFunctions();
		if (GetWorld()) {
			ULocalPlayer* LocalPlayer = GEngine->GetLocalPlayerFromControllerId(GetWorld(), 0);
			if (LocalPlayer && LocalPlayer->ViewportClient) {
				FVector2D MousePosition = LocalPlayer->ViewportClient->GetMousePosition();
				OwningPlayer->aInvOptionPanel->SetPositionInViewport(MousePosition);
			}
		}
	}
	return Super::NativeOnMouseButtonDown(InGeometry, InMouseEvent);
}

void UUMGInventorySlotWidget::NativeOnMouseEnter(const FGeometry& InGeometry, const FPointerEvent& InMouseEvent)
{
	SlotColor = FLinearColor(1, 0.6, 0.6, 1);
	if (GeneralStaticFunctionLibrary::ItemIDExists(ItemID)) {
		FInventoryStruct itemStruct = GeneralStaticFunctionLibrary::ItemMap[ItemID];
		OwningPlayer->aInventoryPanel->ItemName = itemStruct.ItemName;
		OwningPlayer->aInventoryPanel->ItemDescription = itemStruct.ItemDescription;
	}
	Super::OnMouseEnter(InGeometry, InMouseEvent);
}

void UUMGInventorySlotWidget::NativeOnMouseLeave(const FPointerEvent& InMouseEvent)
{
	SlotColor = FLinearColor(1, 1, 1, 1);
	OwningPlayer->aInventoryPanel->ItemName = "";
	OwningPlayer->aInventoryPanel->ItemDescription = "";
	Super::OnMouseLeave(InMouseEvent);
}
