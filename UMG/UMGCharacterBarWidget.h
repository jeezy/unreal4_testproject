// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "UMG/CustomWidgetParent.h"
#include "UMG/UMGCharacterBarSlotWidget.h"
#include "UMGCharacterBarWidget.generated.h"

/**
 * 
 */
UCLASS()
class NEWTHIRDPERSONCODE_API UUMGCharacterBarWidget : public UCustomWidgetParent
{
	GENERATED_BODY()

	public:
		void SetupActions(class ANewThirdPersonCodeCharacter* theCharacter);
};
