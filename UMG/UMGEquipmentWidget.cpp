// Fill out your copyright notice in the Description page of Project Settings.

#include "NewThirdPersonCode.h"
#include "UMGEquipmentWidget.h"

void UUMGEquipmentWidget::GenerateEquipmentIcons(class ANewThirdPersonCodeCharacter* theCharacter) {
	WidgetTree->ForEachWidget([&](UWidget* Widget) {
		UUMGEquipmentSlotWidget* equipSlotWidget = Cast<UUMGEquipmentSlotWidget>(Widget);
		if (equipSlotWidget){
			EEquipmentSlot equipSlot = GeneralStaticFunctionLibrary::ConvertNameToSlot(equipSlotWidget->SlotName);
			equipSlotWidget->OwningPlayer = theCharacter;
			if (theCharacter->HasEquipmentInSlot(equipSlot)) {
				FString itemName = theCharacter->GetEquipmentSlot(equipSlot);
				int32 itemID = GeneralStaticFunctionLibrary::FindItemIDByName(itemName);
				if (GeneralStaticFunctionLibrary::ItemIDExists(itemID))
				{
					FInventoryStruct itemStruct = GeneralStaticFunctionLibrary::ItemMap[itemID];
					equipSlotWidget->ItemID = itemID;
					equipSlotWidget->ItemTexture = itemStruct.ItemImage;
					equipSlotWidget->FunctionDelegate.BindUObject(theCharacter, &ANewThirdPersonCodeCharacter::UnsetEquipmentSlot, equipSlot);
					equipSlotWidget->SlotColor = FLinearColor(1, 1, 1, 1);
				}
			}
			else {
				equipSlotWidget->ItemID = -1;
				equipSlotWidget->ItemTexture = GeneralStaticFunctionLibrary::LoadTextureFromPath("/Game/ThirdPerson/Textures/Icons/T_InvSlotEmpty");
				equipSlotWidget->SlotColor = FLinearColor(1, 1, 1, 0.5);
			}
		}
	});
}


