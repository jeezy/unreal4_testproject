// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "UMG/CustomWidgetParent.h"
#include "Enemy/EnemyPawnParent.h"
#include "Enemy/EnemyCharacterParent.h"
#include "UMGTargetPanel.generated.h"

/**
 * 
 */
UCLASS()
class NEWTHIRDPERSONCODE_API UUMGTargetPanel : public UCustomWidgetParent
{
	GENERATED_BODY()
	
	public:

		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Target Stats")
		float TargetHealthPercent;

		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Target Stats")
		float TargetManaPercent;

		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Target Info")
		FString TargetName;

		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Target Info")
		UTexture2D* TargetTexture;

		class AEnemyCharacterParent* TargetObject;

		FTimerHandle RefreshTargetInfoHandle;

		void BeginTargetInfoRefresh();

		void RefreshTargetInfo();

};
