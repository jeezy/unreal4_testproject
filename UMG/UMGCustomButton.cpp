// Fill out your copyright notice in the Description page of Project Settings.

#include "NewThirdPersonCode.h"
#include "UMGCustomButton.h"

FReply UUMGCustomButton::NativeOnMouseButtonDown(const FGeometry& InGeometry, const FPointerEvent& InMouseEvent) 
{
	if (FunctionDelegate.IsBound())
		FunctionDelegate.Execute();
	return Super::NativeOnMouseButtonDown(InGeometry, InMouseEvent);
}

void UUMGCustomButton::NativeOnMouseEnter(const FGeometry& InGeometry, const FPointerEvent& InMouseEvent)
{
	if (GEngine) {
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, "Entered");
	}
	ButtonColor = FLinearColor(0.45, 0.45, 0.45, 1);
	Super::OnMouseEnter(InGeometry, InMouseEvent);
}

void UUMGCustomButton::NativeOnMouseLeave(const FPointerEvent& InMouseEvent)
{
	if (GEngine) {
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, "Leave");
	}
	ButtonColor = DefaultButtonColor;
	Super::OnMouseLeave(InMouseEvent);
}