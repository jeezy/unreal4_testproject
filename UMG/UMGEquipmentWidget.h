// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "UMG/CustomWidgetParent.h"
#include "NewThirdPersonCodeCharacter.h"
#include "UMG/UMGEquipmentSlotWidget.h"
#include "Game/StaticEnumLibrary.h"
#include "UMGEquipmentWidget.generated.h"

/**
 * 
 */
UCLASS()
class NEWTHIRDPERSONCODE_API UUMGEquipmentWidget : public UCustomWidgetParent
{
	GENERATED_BODY()

	public:
		void GenerateEquipmentIcons(class ANewThirdPersonCodeCharacter* theCharacter);
		
		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Slot Info")
			FString ItemName;

	
};
