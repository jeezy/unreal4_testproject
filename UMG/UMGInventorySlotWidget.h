// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "UMG/CustomWidgetParent.h"
#include "UMG/UMGInventoryOptionMenuWidget.h"
#include "Game/StaticStructLibrary.h"
#include "GeneralStaticFunctionLibrary.h"
#include "NewThirdPersonCodeCharacter.h"
#include "UMGInventorySlotWidget.generated.h"

/**
 * 
 */
UCLASS()
class NEWTHIRDPERSONCODE_API UUMGInventorySlotWidget : public UCustomWidgetParent
{
	GENERATED_BODY()

	public:

		class ANewThirdPersonCodeCharacter* OwningPlayer;

		int32 ItemSlot;

		int32 ItemID;

		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Slot Details")
		FLinearColor SlotColor;

		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item Info")
		FString ItemName;
	
		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item Info")
		UTexture2D* ItemTexture;

		virtual FReply NativeOnMouseButtonDown(const FGeometry& InGeometry, const FPointerEvent& InMouseEvent) override;

		virtual void NativeOnMouseEnter(const FGeometry& InGeometry, const FPointerEvent& InMouseEvent) override;

		virtual void NativeOnMouseLeave(const FPointerEvent& InMouseEvent) override;
	
};
