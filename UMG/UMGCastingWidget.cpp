// Fill out your copyright notice in the Description page of Project Settings.

#include "NewThirdPersonCode.h"
#include "UMGCastingWidget.h"

void UUMGCastingWidget::BeginSpellCasting(float length)
{
	UWorld* theWorld = GetWorld();
	if (theWorld) {
		SpellCastEndTime = theWorld->GetTimeSeconds() + length;
		SpellCastLength = length;
		GetWorld()->GetTimerManager().SetTimer(SpellCastRefreshHandle, this, &UUMGCastingWidget::SpellCastRefresh, 0.05, true);
	}
	
}

void UUMGCastingWidget::SpellCastRefresh()
{
	UWorld* theWorld = GetWorld();
	if (theWorld) {
		if (SpellCastEndTime >= theWorld->GetTimeSeconds()) {
			SpellCastPercent = GetSpellCastPercent();
		}
		else {
			theWorld->GetTimerManager().ClearTimer(SpellCastRefreshHandle);
		}
	}
}

float UUMGCastingWidget::GetSpellCastPercent()
{
	UWorld* theWorld = GetWorld();
	if (theWorld) {
		return FMath::Clamp(((SpellCastEndTime - theWorld->GetTimeSeconds()) / SpellCastLength), 0.F, 1.F);
	}
	else {
		return 0;
	}
}