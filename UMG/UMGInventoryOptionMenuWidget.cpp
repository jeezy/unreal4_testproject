// Fill out your copyright notice in the Description page of Project Settings.

#include "NewThirdPersonCode.h"
#include "UMGInventoryOptionMenuWidget.h"

void UUMGInventoryOptionMenuWidget::ConsumeItem()
{
	OwningPlayer->TheInventory.RemoveAt(ItemSlot);
	OwningPlayer->aInventoryPanel->GenerateInventoryIcons(OwningPlayer);
	FInventoryStruct inventoryStruct = GeneralStaticFunctionLibrary::ItemMap[ItemID];
	if (GEngine) {
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString("Consumed " + inventoryStruct.ItemName));
	}
	inventoryStruct.UseDelegate.Execute();
	OwningPlayer->aInvOptionPanel = NULL;
	this->RemoveFromViewport();
}

void UUMGInventoryOptionMenuWidget::RemoveItem()
{
	OwningPlayer->TheInventory.RemoveAt(ItemSlot);
	OwningPlayer->aInventoryPanel->GenerateInventoryIcons(OwningPlayer);
	if (GEngine) {
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString("Removed Item"));
	}
	OwningPlayer->aInvOptionPanel = NULL;
	this->RemoveFromViewport();
}

void UUMGInventoryOptionMenuWidget::Cancel()
{
	if (GEngine) {
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString("Closed Menu"));
	}
	OwningPlayer->aInvOptionPanel = NULL;
	this->RemoveFromViewport();
}

void UUMGInventoryOptionMenuWidget::SetActionButtonText()
{
	FInventoryStruct itemStruct = GeneralStaticFunctionLibrary::ItemMap[ItemID];
	switch (itemStruct.UseType)
	{
		case EItemUseType::Consume:
			ActionButtonText = "Consume";
		break;
		case EItemUseType::Equip:
			ActionButtonText = "Equip";
		break;
		case EItemUseType::Use:
			ActionButtonText = "Use";
		break;
	}
}

void UUMGInventoryOptionMenuWidget::SetupButtonFunctions()
{
	SetActionButtonText();
	int32 index = 0;
	WidgetTree->ForEachWidget([&](UWidget* Widget) {
		UUMGCustomButton* buttonWidget = Cast<UUMGCustomButton>(Widget);
		if (buttonWidget){
			buttonWidget->DefaultButtonColor = buttonWidget->ButtonColor;
			switch (index)
			{
			case 0: // Consume
				buttonWidget->FunctionDelegate.BindUObject(this, &UUMGInventoryOptionMenuWidget::ConsumeItem);
				break;
			case 1: // Remove
				buttonWidget->FunctionDelegate.BindUObject(this, &UUMGInventoryOptionMenuWidget::RemoveItem);
				break;
			case 2: // Cancel
				buttonWidget->FunctionDelegate.BindUObject(this, &UUMGInventoryOptionMenuWidget::Cancel);
				break;
			default:
				buttonWidget->FunctionDelegate.BindUObject(this, &UUMGInventoryOptionMenuWidget::Cancel);
			}
			index++;
		}
	});
}



