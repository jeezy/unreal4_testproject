// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "UMG/CustomWidgetParent.h"
#include "UMGCastingWidget.generated.h"

/**
 * 
 */
UCLASS()
class NEWTHIRDPERSONCODE_API UUMGCastingWidget : public UCustomWidgetParent
{
	GENERATED_BODY()
	
	public:

		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Spell Time")
		float SpellCastPercent;
	
		float SpellCastLength;
		float SpellCastEndTime;

		FTimerHandle SpellCastRefreshHandle;

		float GetSpellCastPercent();

		void BeginSpellCasting( float length );

		void SpellCastRefresh();
	
};
