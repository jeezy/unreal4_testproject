// Fill out your copyright notice in the Description page of Project Settings.

#include "NewThirdPersonCode.h"
#include "UMGCharacterBarSlotWidget.h"

FReply UUMGCharacterBarSlotWidget::NativeOnMouseButtonDown(const FGeometry& InGeometry, const FPointerEvent& InMouseEvent)
{
	if (FunctionDelegate.IsBound())
		FunctionDelegate.Execute();
	return Super::NativeOnMouseButtonDown(InGeometry, InMouseEvent);
}

void UUMGCharacterBarSlotWidget::NativeOnMouseEnter(const FGeometry& InGeometry, const FPointerEvent& InMouseEvent)
{
	Super::OnMouseEnter(InGeometry, InMouseEvent);
}

void UUMGCharacterBarSlotWidget::NativeOnMouseLeave(const FPointerEvent& InMouseEvent)
{
	Super::OnMouseLeave(InMouseEvent);
}


