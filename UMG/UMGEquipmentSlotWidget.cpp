// Fill out your copyright notice in the Description page of Project Settings.

#include "NewThirdPersonCode.h"
#include "UMGEquipmentSlotWidget.h"

FReply UUMGEquipmentSlotWidget::NativeOnMouseButtonDown(const FGeometry& InGeometry, const FPointerEvent& InMouseEvent)
{
	if (FunctionDelegate.IsBound())
	{
		FunctionDelegate.Execute();
		OwningPlayer->aEquipmentPanel->GenerateEquipmentIcons(OwningPlayer);
	}
	return Super::NativeOnMouseButtonDown(InGeometry, InMouseEvent);
}

void UUMGEquipmentSlotWidget::NativeOnMouseEnter(const FGeometry& InGeometry, const FPointerEvent& InMouseEvent)
{
	if (GeneralStaticFunctionLibrary::ItemIDExists(ItemID))
	{
		FInventoryStruct itemStruct = GeneralStaticFunctionLibrary::ItemMap[ItemID];
		OwningPlayer->aEquipmentPanel->ItemName = itemStruct.ItemName;
	}
	Super::OnMouseEnter(InGeometry, InMouseEvent);
}

void UUMGEquipmentSlotWidget::NativeOnMouseLeave(const FPointerEvent& InMouseEvent)
{
	OwningPlayer->aEquipmentPanel->ItemName = "";
	Super::OnMouseLeave(InMouseEvent);
}


