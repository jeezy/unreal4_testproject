// Fill out your copyright notice in the Description page of Project Settings.

#include "NewThirdPersonCode.h"
#include "UMGInventoryWidget.h"

void UUMGInventoryWidget::GenerateInventoryIcons(class ANewThirdPersonCodeCharacter* theCharacter) {
	int32 index = 0;
	WidgetTree->ForEachWidget([&](UWidget* Widget) {
		UUMGInventorySlotWidget* inventorySlotWidget = Cast<UUMGInventorySlotWidget>(Widget);
		if (inventorySlotWidget){
			inventorySlotWidget->OwningPlayer = theCharacter;
			if (theCharacter->TheInventory.IsValidIndex(index)) {
				if (GeneralStaticFunctionLibrary::ItemIDExists(theCharacter->TheInventory[index]))
				{
					FInventoryStruct itemStruct = GeneralStaticFunctionLibrary::ItemMap[theCharacter->TheInventory[index]];
					inventorySlotWidget->ItemSlot = index;
					inventorySlotWidget->ItemID = theCharacter->TheInventory[index];
					inventorySlotWidget->ItemName = itemStruct.ItemName;
					inventorySlotWidget->ItemTexture = itemStruct.ItemImage;
				}
			}
			else {
				inventorySlotWidget->ItemSlot = -1;
				inventorySlotWidget->ItemID = -1;
				inventorySlotWidget->ItemName = "Empty";
				inventorySlotWidget->ItemTexture = GeneralStaticFunctionLibrary::LoadTextureFromPath("/Game/ThirdPerson/Textures/Icons/T_InvSlotEmpty");
			}
			index++;
		}
	});
}