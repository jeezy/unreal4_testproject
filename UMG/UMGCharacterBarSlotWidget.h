// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "UMG/CustomWidgetParent.h"
#include "NewThirdPersonCodeCharacter.h"
#include "UMGCharacterBarSlotWidget.generated.h"

DECLARE_DELEGATE(FFunctionDelegate);

UCLASS()
class NEWTHIRDPERSONCODE_API UUMGCharacterBarSlotWidget : public UCustomWidgetParent
{
	GENERATED_BODY()
	
	public:

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Action Info")
	FString SlotName;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Action Info")
	UTexture2D* ItemTexture;
	
	FFunctionDelegate FunctionDelegate;

	virtual FReply NativeOnMouseButtonDown(const FGeometry& InGeometry, const FPointerEvent& InMouseEvent) override;

	virtual void NativeOnMouseEnter(const FGeometry& InGeometry, const FPointerEvent& InMouseEvent) override;

	virtual void NativeOnMouseLeave(const FPointerEvent& InMouseEvent) override;
};
