// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Blueprint/UserWidget.h"
#include "Runtime/UMG/Public/UMG.h"
#include "Runtime/UMG/Public/UMGStyle.h"
#include "Runtime/UMG/Public/Slate/SObjectWidget.h"
#include "Runtime/UMG/Public/IUMGModule.h"
#include "Runtime/UMG/Public/Blueprint/UserWidget.h"
#include "Engine.h"
#include "CustomWidgetParent.generated.h"

/**
 * 
 */
UCLASS()
class NEWTHIRDPERSONCODE_API UCustomWidgetParent : public UUserWidget
{
	GENERATED_BODY()
	
	public:

		bool IsDragging;

		float NextDragTime;

		float TimeUntilMove;

		float NextClickToDrag;

		FVector2D LastClickPos;

		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Custom Widget")
			FString CustomWidgetName;

		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Custom Widget")
			bool CanDrag;

		FVector2D GetMousePos(UWorld* theWorld);

		virtual FReply NativeOnMouseButtonDown(const FGeometry& InGeometry, const FPointerEvent& InMouseEvent) override;

		virtual FReply NativeOnMouseButtonUp(const FGeometry& InGeometry, const FPointerEvent& InMouseEvent) override;

		virtual FReply NativeOnMouseMove(const FGeometry& InGeometry, const FPointerEvent& InMouseEvent) override;

		virtual void NativeTick(const FGeometry& MyGeometry, float InDeltaTime) override;

};
