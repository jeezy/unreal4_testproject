// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.

#include "NewThirdPersonCode.h"
#include "NewThirdPersonCodeGameMode.h"
#include "NewThirdPersonCodeCharacter.h"

ANewThirdPersonCodeGameMode::ANewThirdPersonCodeGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPerson/Blueprints/ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
