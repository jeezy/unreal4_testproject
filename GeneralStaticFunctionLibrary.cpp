// Fill out your copyright notice in the Description page of Project Settings.

#include "NewThirdPersonCode.h"
#include "GeneralStaticFunctionLibrary.h"

TMap<int32, struct FInventoryStruct> GeneralStaticFunctionLibrary::ItemMap = TMap<int32, struct FInventoryStruct>();
TMap<FString, struct FEquipableStruct> GeneralStaticFunctionLibrary::EquipmentDataMap = TMap<FString, struct FEquipableStruct>();

UFUNCTION()
void SayItemName(FString text)
{
	if (GEngine) {
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, text);
	}
}

GeneralStaticFunctionLibrary::GeneralStaticFunctionLibrary()
{
}

GeneralStaticFunctionLibrary::~GeneralStaticFunctionLibrary()
{
}

void GeneralStaticFunctionLibrary::SetEquipableItemData(FString itemName, FVector posOffset, FVector angOffset, FVector scale, FName socketName)
{
	FEquipableStruct equipStruct;
	equipStruct.ItemPosOffset = posOffset;
	equipStruct.ItemAngOffset = angOffset;
	equipStruct.ItemScale = scale;
	equipStruct.ItemSocketName = socketName;
	EquipmentDataMap.Add(itemName, equipStruct);
}

void GeneralStaticFunctionLibrary::GenerateEquipableMap()
{
	SetEquipableItemData("Viking Helmet", FVector(0, 0, 13), FVector(0, 0, 0), FVector(0.26, 0.26, 0.26), FName(TEXT("HelmetSocket")));
}

void GeneralStaticFunctionLibrary::CreateInventoryItem(class ANewThirdPersonCodeCharacter* theCharacter, FString itemName, FString itemDescription, FString imagePath, EItemUseType useType, EItemType itemType)
{
	FInventoryStruct theItem;
	theItem.ItemName = itemName;
	theItem.ItemDescription = itemDescription;
	theItem.ItemImage = LoadTextureFromPath(imagePath);
	switch (useType) 
	{
		case EItemUseType::Consume:
			theItem.UseDelegate.BindUObject(theCharacter, &ANewThirdPersonCodeCharacter::ConsumeFood, itemName);
		break;
		case EItemUseType::Equip:
			theItem.UseDelegate.BindUObject(theCharacter, &ANewThirdPersonCodeCharacter::EquipItem, itemName, itemType);
		break;
		case EItemUseType::Use:
			theItem.UseDelegate.BindUObject(theCharacter, &ANewThirdPersonCodeCharacter::UseItem, itemName, itemType);
		break;
	}
	theItem.UseType = useType;
	theItem.ItemType = itemType;
	ItemMap.Add(ItemMap.Num(), theItem);
}

void GeneralStaticFunctionLibrary::GenerateItemMap(class ANewThirdPersonCodeCharacter* theCharacter)
{
	CreateInventoryItem(theCharacter, "Burger", "Ooh that looks tasty!", "/Game/ThirdPerson/Textures/Icons/T_InvSlotBurger", EItemUseType::Consume, EItemType::Burger);
	CreateInventoryItem(theCharacter, "Water", "Mhmm that water would hit the spot.", "/Game/ThirdPerson/Textures/Icons/T_InvSlotWater", EItemUseType::Consume, EItemType::Water);
	CreateInventoryItem(theCharacter, "Viking Helmet", "It looks worn out, but still stylish.", "/Game/ThirdPerson/Textures/Icons/T_InvSlotVikingHelmet", EItemUseType::Equip, EItemType::Helmet);
}

bool GeneralStaticFunctionLibrary::Trace(AActor* ActorToIgnore, const FVector& Start, const FVector& End, FHitResult& HitOut, ECollisionChannel CollisionChannel, bool ReturnPhysMat)
{
	FCollisionQueryParams TraceParams(FName(TEXT("VictoreCore Trace")), true, ActorToIgnore);
	TraceParams.bTraceComplex = true;
	//TraceParams.bTraceAsyncScene = true;
	TraceParams.bReturnPhysicalMaterial = ReturnPhysMat;

	//Ignore Actors
	TraceParams.AddIgnoredActor(ActorToIgnore);

	//Re-initialize hit info
	HitOut = FHitResult(ForceInit);

	//Get World Context
	TObjectIterator< APlayerController > ThePC;
	if (!ThePC) return false;

	//Trace!
	ThePC->GetWorld()->LineTraceSingle(
		HitOut,		//result
		Start,	//start
		End, //end
		CollisionChannel, //collision channel
		TraceParams
		);

	//Hit any Actor?
	return (HitOut.GetActor() != NULL);
}

UTexture2D* GeneralStaticFunctionLibrary::LoadTextureFromPath(const FString& APath)
{
	return Cast<UTexture2D>(StaticLoadObject(UTexture2D::StaticClass(), NULL, *(APath)));
}

UMaterial* GeneralStaticFunctionLibrary::LoadMaterialFromPath(const FString& APath)
{
	return Cast<UMaterial>(StaticLoadObject(UMaterial::StaticClass(), NULL, *(APath)));
}

UStaticMesh* GeneralStaticFunctionLibrary::LoadStaticMeshFromPath(const FString& APath)
{
	return Cast<UStaticMesh>(StaticLoadObject(UStaticMesh::StaticClass(), NULL, *(APath)));
}

EEquipmentSlot GeneralStaticFunctionLibrary::ConvertTypeEnum(EItemType itemType) 
{
	switch (itemType)
	{
		case EItemType::Helmet:
			return EEquipmentSlot::Helmet;
	}
	return EEquipmentSlot::Helmet;
}

EEquipmentSlot GeneralStaticFunctionLibrary::ConvertNameToSlot(FString slotName)
{
	if (slotName == "Helmet")
		return EEquipmentSlot::Helmet;
	else if (slotName == "Shoulders")
		return EEquipmentSlot::Shoulders;
	else if (slotName == "Back")
		return EEquipmentSlot::Back;
	else if (slotName == "Chest")
		return EEquipmentSlot::Chest;
	else if (slotName == "LeftHand")
		return EEquipmentSlot::LeftHand;
	else if (slotName == "RightHand")
		return EEquipmentSlot::RightHand;
	return EEquipmentSlot::Helmet;
}

int32 GeneralStaticFunctionLibrary::FindItemIDByName(FString itemName)
{
	int32 id = -1;
	for (auto It = ItemMap.CreateConstIterator(); It; ++It)
	{
		FInventoryStruct itemStruct = It.Value();
		if (itemStruct.ItemName == itemName)
		{
			id = It.Key();
			break;
		}
	}
	return id;
}

bool GeneralStaticFunctionLibrary::ItemIDExists(int32 itemID)
{
	if (itemID == -1) return false;
	return (ItemMap.Contains(itemID));
}

FVector GeneralStaticFunctionLibrary::GetRandomSpawnPoint(UWorld* theWorld)
{
	FActorIterator AllActorsItr = FActorIterator(theWorld);
	TArray<FVector> waypointArray;
	while (AllActorsItr)
	{
		bool isRespawnPoint = AllActorsItr->ActorHasTag(FName(TEXT("RespawnPoint")));
		if (isRespawnPoint)
			waypointArray.Push(AllActorsItr->GetActorLocation());
		++AllActorsItr;
	}
	return (FVector)RandomArrayValue(waypointArray);
}

template <class T>
T GeneralStaticFunctionLibrary::RandomArrayValue(TArray<T> theArray)
{
	return theArray[FMath::RandRange(0, theArray.Num() - 1)];
}

FVector2D GeneralStaticFunctionLibrary::GetMousePos(UWorld* theWorld)
{
	UEngine* theEngine = GEngine;
	if (theEngine && theWorld)
	{
		ULocalPlayer* LocalPlayer = theEngine->GetLocalPlayerFromControllerId(theWorld, 0);
		FVector2D mousePosition = LocalPlayer->ViewportClient->GetMousePosition();
		return mousePosition;
	}
	return FVector2D(0, 0);
}

void GeneralStaticFunctionLibrary::ApplyForce(AActor* theActor, FVector dir, float force)
{
	ACharacter* theCharacter = Cast<ACharacter>(theActor);
	if (theCharacter)
	{
		theCharacter->GetCharacterMovement()->AddImpulse(dir * force, true);
	}
}

void GeneralStaticFunctionLibrary::ToggleRagdoll(USkeletalMeshComponent* theMesh, AActor* theActor)
{
	if (theMesh->IsSimulatingPhysics())
	{
		theMesh->PutAllRigidBodiesToSleep();
		theMesh->SetAllBodiesSimulatePhysics(false);
		theMesh->SetSimulatePhysics(false);
		theMesh->SetCollisionProfileName(FName(TEXT("NoCollision")));
		theMesh->AttachParent = theActor->GetRootComponent();
		theMesh->SetRelativeLocation(FVector(0, 0, -95));
		theMesh->SetRelativeRotation(FQuat::MakeFromEuler(FVector(0, 0, 270)));
	}
	else
	{
		theMesh->DetachFromParent(true);
		theMesh->SetAllBodiesSimulatePhysics(true);
		theMesh->SetSimulatePhysics(true);
		theMesh->WakeAllRigidBodies();
		theMesh->SetCollisionProfileName(FName(TEXT("Ragdoll")));
	}
}

bool GeneralStaticFunctionLibrary::IsActorInLineOfSight(AActor* theActor, AActor* otherActor, float distance)
{
	FHitResult hitOut = FHitResult(ForceInit);
	bool didHit = Trace(theActor, theActor->GetActorLocation(), theActor->GetActorLocation() + (theActor->GetActorForwardVector() * distance), hitOut, ECollisionChannel::ECC_Pawn, false);
	return (hitOut.GetActor() == otherActor);
}