// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Pawn.h"
#include "EnemyPawnParent.generated.h"

UCLASS()
class NEWTHIRDPERSONCODE_API AEnemyPawnParent : public APawn
{
	GENERATED_BODY()

	int32 EnemyHealth;

	int32 EnemyMana;

public:
	// Sets default values for this pawn's properties
	AEnemyPawnParent();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Enemy Stats")
	int32 EnemyMaxHealth;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Enemy Stats")
	int32 EnemyMaxMana;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Enemy Info")
	UTexture2D* EnemyTexture;

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;

	void InflictDamage(int32 damageAmount, AActor* theInflictor);
	
	float GetHealthPercentage();

	float GetManaPercentage();
};
