// Fill out your copyright notice in the Description page of Project Settings.

#include "NewThirdPersonCode.h"
#include "Enemy/EnemyPawnAIControllerParent.h"
#include "EnemyPawnParent.h"

// Sets default values
AEnemyPawnParent::AEnemyPawnParent()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	AIControllerClass = AEnemyPawnAIControllerParent::StaticClass();
	AutoPossessAI = EAutoPossessAI::PlacedInWorldOrSpawned;
}

// Called when the game starts or when spawned
void AEnemyPawnParent::BeginPlay()
{
	Super::BeginPlay();
	EnemyHealth = EnemyMaxHealth;
	EnemyMana = EnemyMaxMana;
}

// Called every frame
void AEnemyPawnParent::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}

// Called to bind functionality to input
void AEnemyPawnParent::SetupPlayerInputComponent(class UInputComponent* InputComponent)
{
	Super::SetupPlayerInputComponent(InputComponent);

}

float AEnemyPawnParent::GetHealthPercentage()
{
	float healthPercent = (float)EnemyHealth / (float)EnemyMaxHealth;
	return healthPercent;
}

float AEnemyPawnParent::GetManaPercentage()
{
	return (float)EnemyMana / (float)EnemyMaxMana;
}

void AEnemyPawnParent::InflictDamage(int32 damageAmount, AActor* theInflictor)
{
	/*UNavigationSystem* const navSys = GetWorld()->GetNavigationSystem();
	navSys->SimpleMoveToLocation(Controller, GetActorLocation() + FVector(0, 300, 0));
	EnemyHealth = FMath::Clamp(EnemyHealth - damageAmount, 0, EnemyMaxHealth);
	if (EnemyHealth <= 0) {
		this->Destroy();
	}*/
}