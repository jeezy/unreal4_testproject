// Fill out your copyright notice in the Description page of Project Settings.

#include "NewThirdPersonCode.h"
#include "EnemyCharacterParent.h"


// Sets default values
AEnemyCharacterParent::AEnemyCharacterParent()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AEnemyCharacterParent::BeginPlay()
{
	Super::BeginPlay();
	EnemyHealth = EnemyMaxHealth;
	EnemyMana = EnemyMaxMana;
	NavSys = GetWorld()->GetNavigationSystem();
	EnemyState = EEnemyState::Roaming;
	TheWorld = GetWorld();
	GoalPosition = GetRandomPos(RoamRange);
	NavSys->SimpleMoveToLocation(Controller, GoalPosition);
	ExtendWaypointLife();
}

// Called every frame
void AEnemyCharacterParent::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

	EEnemyState oldState = EnemyState;
	ANewThirdPersonCodeCharacter* theCharacter = Cast<ANewThirdPersonCodeCharacter>(aTarget);
	if (theCharacter && theCharacter->IsDead)
		aTarget = NULL;
	switch (EnemyState)
	{
		case EEnemyState::Roaming:
			if (IsNearWaypoint()) {
				GoalPosition = GetRandomPos(RoamRange);
				NavSys->SimpleMoveToLocation(Controller, GoalPosition);
				ExtendWaypointLife();
			}
			else if (IsWaypointExpired()) {
				GoalPosition = GetRandomPos(RoamRange);
				NavSys->SimpleMoveToLocation(Controller, GoalPosition);
				ExtendWaypointLife();
			}
		break;
		case EEnemyState::Chasing:
			if (aTarget)
			{
				if (IsTargetInRange()) {
					if (IsInAttackRange()) {
						GoalPosition = GetActorLocation();
						EnemyState = EEnemyState::Attacking;
					}
					else {
						GoalPosition = aTarget->GetActorLocation();
						NavSys->SimpleMoveToLocation(Controller, GoalPosition);
						if (IsWaypointExpired())
							ExtendWaypointLife();
					}
				}
				else {
					EnemyState = EEnemyState::Roaming;
					GoalPosition = GetRandomPos(RoamRange);
					NavSys->SimpleMoveToLocation(Controller, GoalPosition);
					ExtendWaypointLife();
					aTarget = NULL;
				}
			}
			else {
				EnemyState = EEnemyState::Roaming;
				GoalPosition = GetRandomPos(RoamRange);
				NavSys->SimpleMoveToLocation(Controller, GoalPosition);
				ExtendWaypointLife();
			}
		break;
		case EEnemyState::Attacking:
			if (IsInAttackRange()) {
				GoalPosition = GetActorLocation();
				NavSys->SimpleMoveToLocation(Controller, GoalPosition);
				FRotator theRot = FRotationMatrix::MakeFromX(aTarget->GetActorLocation() - GetActorLocation()).Rotator();
				SetActorRotation(theRot);
				if (CanMeleeAttack())
					CommenceMeleeAttack();
				if (IsWaypointExpired())
					ExtendWaypointLife();
			}
			else {
				EnemyState = EEnemyState::Chasing;
				GoalPosition = GetActorLocation();
				NavSys->SimpleMoveToLocation(Controller, GoalPosition);
				ExtendWaypointLife();
			}
		break;
		case EEnemyState::Fleeing:
			//NOT IMPLEMENTED
		break;
	}
	/*if ( IsWaypointExpired() || oldState != EnemyState) {
		NavSys->SimpleMoveToLocation(Controller, GoalPosition);
		ExtendWaypointLife();
	}*/
}

// Called to bind functionality to input
void AEnemyCharacterParent::SetupPlayerInputComponent(class UInputComponent* InputComponent)
{
	Super::SetupPlayerInputComponent(InputComponent);
}

float AEnemyCharacterParent::GetHealthPercentage()
{
	float healthPercent = (float)EnemyHealth / (float)EnemyMaxHealth;
	return healthPercent;
}

float AEnemyCharacterParent::GetManaPercentage()
{
	return (float)EnemyMana / (float)EnemyMaxMana;
}

void AEnemyCharacterParent::InflictDamage(int32 damageAmount, AActor* theInflictor)
{
	aTarget = theInflictor;
	GoalPosition = aTarget->GetActorLocation();
	EnemyState = EEnemyState::Chasing;
	EnemyHealth = FMath::Clamp(EnemyHealth - damageAmount, 0, EnemyMaxHealth);
	if (EnemyHealth <= 0) {
		this->Destroy();
	}
}

FVector AEnemyCharacterParent::GetRandomPos(float range)
{
	return GetActorLocation() + FVector(FMath::RandRange(-range, range), FMath::RandRange(-range, range), 0);
}

bool AEnemyCharacterParent::IsNearWaypoint()
{
	return ((FVector::Dist(GetActorLocation(), GoalPosition)) <= WaypointTolerance);
}

bool AEnemyCharacterParent::IsTargetInRange()
{
	if (!aTarget) return false;
	return (FVector::Dist(GetActorLocation(), aTarget->GetActorLocation()) < ChaseRange);
}

bool AEnemyCharacterParent::IsInAttackRange()
{
	if (!aTarget) return false;
	return (FVector::Dist(GetActorLocation(), aTarget->GetActorLocation()) < AttackRange);
}

bool AEnemyCharacterParent::IsWaypointExpired()
{
	return (WaypointExpireTime <= TheWorld->GetTimeSeconds());
}

void AEnemyCharacterParent::ExtendWaypointLife()
{
	WaypointExpireTime = TheWorld->GetTimeSeconds() + WaypointLifeTime;
}

bool AEnemyCharacterParent::CanMeleeAttack()
{
	return (IsInAttackRange() && NextMeleeAttack < TheWorld->GetRealTimeSeconds() && GeneralStaticFunctionLibrary::IsActorInLineOfSight(this, aTarget, 500));
}

void AEnemyCharacterParent::CommenceMeleeAttack()
{
	NextMeleeAttack = TheWorld->GetRealTimeSeconds() + MeleeAttackCooldown;
	ANewThirdPersonCodeCharacter* theCharacter = Cast<ANewThirdPersonCodeCharacter>(aTarget);
	if (theCharacter) 
	{
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString("Executed Attack"));
		int32 damageToInflict = FMath::Round(FMath::FRandRange(MeleeDamageRange.X, MeleeDamageRange.Y));
		theCharacter->InflictDamage(damageToInflict, this);
	}
}