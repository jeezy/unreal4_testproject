// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Character.h"
#include "GeneralStaticFunctionLibrary.h"
#include "NewThirdPersonCodeCharacter.h"
#include "EnemyCharacterParent.generated.h"

UENUM(BlueprintType)
enum class EEnemyState : uint8
{
	Roaming UMETA(DisplayName = "Roaming"),
	Chasing UMETA(DisplayName = "Chasing"),
	Attacking UMETA(DisplayName = "Attacking"),
	Fleeing UMETA(DisplayName = "Fleeiing")
};

UCLASS()
class NEWTHIRDPERSONCODE_API AEnemyCharacterParent : public ACharacter
{
	GENERATED_BODY()

	int32 EnemyHealth;

	int32 EnemyMana;

	FVector GoalPosition;

	UNavigationSystem* NavSys;

	AActor* aTarget;

	EEnemyState EnemyState;

	float WaypointExpireTime;

	UWorld* TheWorld;

	float NextMeleeAttack;

public:

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Enemy Stats")
		FVector2D MeleeDamageRange;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Enemy Stats")
		int32 MeleeAttackCooldown;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Enemy Stats")
		int32 EnemyMaxHealth;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Enemy Stats")
		int32 EnemyMaxMana;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Enemy Info")
		UTexture2D* EnemyTexture;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Enemy AI")
		float WaypointLifeTime;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Enemy AI")
		float RoamRange;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Enemy AI")
		float WaypointTolerance;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Enemy AI")
		float ChaseRange;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Enemy AI")
		float AttackRange;

	// Sets default values for this character's properties
	AEnemyCharacterParent();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;

	void InflictDamage(int32 damageAmount, AActor* theInflictor);

	float GetHealthPercentage();

	float GetManaPercentage();

	FVector GetRandomPos(float range);

	bool IsNearWaypoint();

	bool IsTargetInRange();

	bool IsInAttackRange();
	
	bool IsWaypointExpired();

	void ExtendWaypointLife();

	bool CanMeleeAttack();

	void CommenceMeleeAttack();
};
