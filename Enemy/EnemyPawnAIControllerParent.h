// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "AIController.h"
#include "EnemyPawnAIControllerParent.generated.h"

/**
 * 
 */
UCLASS()
class NEWTHIRDPERSONCODE_API AEnemyPawnAIControllerParent : public AAIController
{
	GENERATED_BODY()
	
	public:
		AEnemyPawnAIControllerParent();

		void MoveToDesiredPos(FVector pos);
	
	
};
