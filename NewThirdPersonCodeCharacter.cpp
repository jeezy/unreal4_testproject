// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.

#include "NewThirdPersonCode.h"
#include "NewThirdPersonCodeCharacter.h"
#include "GeneralStaticFunctionLibrary.h"
#include "Engine.h"

//////////////////////////////////////////////////////////////////////////
// ANewThirdPersonCodeCharacter

ANewThirdPersonCodeCharacter::ANewThirdPersonCodeCharacter()
{

	PrimaryActorTick.bCanEverTick = true;

	FirstPersonToggled = false;

	BaseWalkSpeed = GetCharacterMovement()->MaxWalkSpeed;

	RunSpeedMulti = 3;
	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);
	// set our turn rates for input
	BaseTurnRate = 45.f;
	BaseLookUpRate = 45.f;

	// Don't rotate when the controller rotates. Let that just affect the camera.
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Character moves in the direction of input...	
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 540.0f, 0.0f); // ...at this rotation rate
	GetCharacterMovement()->JumpZVelocity = 600.f;
	GetCharacterMovement()->AirControl = 0.2f;

	// Create a camera boom (pulls in towards the player if there is a collision)
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->AttachTo(RootComponent);
	CameraBoom->TargetArmLength = 300.0f; // The camera follows at this distance behind the character	
	CameraBoom->bUsePawnControlRotation = true; // Rotate the arm based on the controller
	LastArmLength = CameraBoom->TargetArmLength;

	// Create a follow camera
	FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
	FollowCamera->AttachTo(CameraBoom, USpringArmComponent::SocketName); // Attach the camera to the end of the boom and let the boom adjust to match the controller orientation
	FollowCamera->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	// Note: The skeletal mesh and anim blueprint references on the Mesh component (inherited from Character) 
	// are set in the derived blueprint asset named MyCharacter (to avoid direct content references in C++)
}


void ANewThirdPersonCodeCharacter::BeginPlay()
{
	Super::BeginPlay();
	GeneralStaticFunctionLibrary::GenerateItemMap(this);
	GeneralStaticFunctionLibrary::GenerateEquipableMap();
	Health = MaxHealth;
	Mana = MaxMana;
	if ( GEngine && Controller ) {
		Controller->CastToPlayerController()->bShowMouseCursor = true;
		UCustomWidgetParent* targetWidget = CreateWidget<UCustomWidgetParent>(Controller->CastToPlayerController(), TargetWidgetPanelClass);
		targetWidget->AddToViewport(0);
		aTargetPanel = Cast<UUMGTargetPanel>(targetWidget);
		aTargetPanel->BeginTargetInfoRefresh();
		aTargetPanel->SetVisibility(ESlateVisibility::Hidden);
		UCustomWidgetParent* castingWidget = CreateWidget<UCustomWidgetParent>(Controller->CastToPlayerController(), CastingWidgetPanelClass);
		castingWidget->AddToViewport(0);
		aCastingPanel = Cast<UUMGCastingWidget>(castingWidget);
		aCastingPanel->SetVisibility(ESlateVisibility::Hidden);
		TheInventory.Push(0);
		TheInventory.Push(1);
		TheInventory.Push(1);
		TheInventory.Push(2);
		TheInventory.Push(0);
		UCustomWidgetParent* inventoryWidget = CreateWidget<UCustomWidgetParent>(Controller->CastToPlayerController(), InventoryWidgetPanelClass);
		inventoryWidget->AddToViewport(0);
		aInventoryPanel = Cast<UUMGInventoryWidget>(inventoryWidget);
		aInventoryPanel->GenerateInventoryIcons(this);
		aInventoryPanel->SetVisibility(ESlateVisibility::Hidden);
		UCustomWidgetParent* equipmentWidget = CreateWidget<UCustomWidgetParent>(Controller->CastToPlayerController(), EquipmentWidgetPanelClass);
		equipmentWidget->AddToViewport(0);
		aEquipmentPanel = Cast<UUMGEquipmentWidget>(equipmentWidget);
		aEquipmentPanel->GenerateEquipmentIcons(this);
		aEquipmentPanel->SetVisibility(ESlateVisibility::Hidden);
		UCustomWidgetParent* characterBarWidget = CreateWidget<UCustomWidgetParent>(Controller->CastToPlayerController(), CharacterBarWidgetPanelClass);
		characterBarWidget->AddToViewport(0);
		aCharacterBarPanel = Cast<UUMGCharacterBarWidget>(characterBarWidget);
		aCharacterBarPanel->SetupActions(this);
	}
}

void ANewThirdPersonCodeCharacter::Tick(float deltaSeconds)
{
	Super::Tick(deltaSeconds);

	UWorld* theWorld = GetWorld();
	if (CanMeleeAttack())
	{
		NextAttackTime = theWorld->GetTimeSeconds() + AttackCooldown;
		if (!IsFacingTarget()){
			GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString("Not facing target"));
			return;
		}
		if (FVector::Dist(theTarget->GetActorLocation(), this->GetActorLocation()) < AttackRange) {
			AEnemyCharacterParent* anEnemy = Cast<AEnemyCharacterParent>(theTarget);
			if (anEnemy) {
				AActor* thisActor = Cast<AActor>(this);
				anEnemy->InflictDamage(FMath::RandRange((int32)AttackDamageRange.X, (int32)AttackDamageRange.Y), thisActor);
				GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString("Attacking"));
				IsPunching = true;
				theWorld->GetTimerManager().SetTimer(PunchEndHandle, this, &ANewThirdPersonCodeCharacter::EndPunching, 0.5, false);
			}
		}
		else {
			GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString("Too far away"));
		}
	}
	if (aTargetPanel && aTargetPanel->GetVisibility() == ESlateVisibility::Visible && aTargetPanel->TargetHealthPercent == 0) {
		DeselectTarget();
	}
}

void ANewThirdPersonCodeCharacter::EndPunching()
{
	IsPunching = false;
}

//////////////////////////////////////////////////////////////////////////
// Input

void ANewThirdPersonCodeCharacter::SetupPlayerInputComponent(class UInputComponent* InputComponent)
{
	// Set up gameplay key bindings
	check(InputComponent);
	InputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	InputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);

	InputComponent->BindAction("Use", IE_Pressed, this, &ANewThirdPersonCodeCharacter::Use);

	InputComponent->BindAction("LeftClick", IE_Pressed, this, &ANewThirdPersonCodeCharacter::LeftMousePress);
	InputComponent->BindAction("LeftClick", IE_Released, this, &ANewThirdPersonCodeCharacter::LeftMouseReleased);

	InputComponent->BindAction("RightClick", IE_Pressed, this, &ANewThirdPersonCodeCharacter::RightMousePress);

	InputComponent->BindAction("ToggleView", IE_Pressed, this, &ANewThirdPersonCodeCharacter::ToggleView);

	InputComponent->BindAction("HotKeyOne", IE_Pressed, this, &ANewThirdPersonCodeCharacter::HotKeyOne);

	InputComponent->BindAction("Sprint", IE_Pressed, this, &ANewThirdPersonCodeCharacter::StartSprinting);
	InputComponent->BindAction("Sprint", IE_Released, this, &ANewThirdPersonCodeCharacter::StopSprinting);

	InputComponent->BindAction("ScrollTowards", IE_Pressed, this, &ANewThirdPersonCodeCharacter::ScrollTowards);
	InputComponent->BindAction("ScrollAway", IE_Pressed, this, &ANewThirdPersonCodeCharacter::ScrollAway);

	InputComponent->BindAxis("MoveForward", this, &ANewThirdPersonCodeCharacter::MoveForward);
	InputComponent->BindAxis("MoveRight", this, &ANewThirdPersonCodeCharacter::MoveRight);

	// We have 2 versions of the rotation bindings to handle different kinds of devices differently
	// "turn" handles devices that provide an absolute delta, such as a mouse.
	// "turnrate" is for devices that we choose to treat as a rate of change, such as an analog joystick
	InputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	InputComponent->BindAxis("TurnRate", this, &ANewThirdPersonCodeCharacter::TurnAtRate);
	InputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
	InputComponent->BindAxis("LookUpRate", this, &ANewThirdPersonCodeCharacter::LookUpAtRate);

	// handle touch devices
	InputComponent->BindTouch(IE_Pressed, this, &ANewThirdPersonCodeCharacter::TouchStarted);
	InputComponent->BindTouch(IE_Released, this, &ANewThirdPersonCodeCharacter::TouchStopped);
}


void ANewThirdPersonCodeCharacter::TouchStarted(ETouchIndex::Type FingerIndex, FVector Location)
{
	// jump, but only on the first touch
	if (FingerIndex == ETouchIndex::Touch1)
	{
		CancelSpellCharging();
		Jump();
	}
}

void ANewThirdPersonCodeCharacter::TouchStopped(ETouchIndex::Type FingerIndex, FVector Location)
{
	if (FingerIndex == ETouchIndex::Touch1)
	{
		StopJumping();
	}
}

void ANewThirdPersonCodeCharacter::TurnAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

void ANewThirdPersonCodeCharacter::LookUpAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}

void ANewThirdPersonCodeCharacter::MoveForward(float Value)
{
	if ((Controller != NULL) && (Value != 0.0f))
	{
		CancelSpellCharging();
		// find out which way is forward
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get forward vector
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
		AddMovementInput(Direction, Value);
	}
}

void ANewThirdPersonCodeCharacter::MoveRight(float Value)
{
	if ( (Controller != NULL) && (Value != 0.0f) )
	{
		CancelSpellCharging();
		// find out which way is right
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);
	
		// get right vector 
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
		// add movement in that direction
		AddMovementInput(Direction, Value);
	}
}

void ANewThirdPersonCodeCharacter::HotKeyOne()
{
	UWorld* theWorld = GetWorld();
	if (CanCastSpell()) {
		if (IsFacingTarget()) {
			if (FVector::Dist(theTarget->GetActorLocation(), this->GetActorLocation()) < SpellRange) {
				IsChargingSpell = true;
				theWorld->GetTimerManager().SetTimer(ChargeSpellHandle, this, &ANewThirdPersonCodeCharacter::EndChargingSpell, SpellCastTime, false);
				aCastingPanel->SetVisibility(ESlateVisibility::Visible);
				aCastingPanel->BeginSpellCasting(SpellCastTime);
			}
			else {
				GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString("Out of range"));
			}
		}
		else {
			GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString("Not facing target"));
		}
	}
}

void ANewThirdPersonCodeCharacter::CancelSpellCharging()
{
	aCastingPanel->SetVisibility(ESlateVisibility::Hidden);
	IsChargingSpell = false;
	GetWorld()->GetTimerManager().ClearTimer(ChargeSpellHandle);
}

void ANewThirdPersonCodeCharacter::EndChargingSpell()
{
	if (theTarget) {
		UWorld* theWorld = GetWorld();
		NextSpellTime = theWorld->GetTimeSeconds() + SpellCooldown;
		FActorSpawnParameters spawnParams;
		spawnParams.Owner = this;
		spawnParams.Instigator = Instigator;
		AProjectileActorParent* aProjectile = GetWorld()->SpawnActor<AProjectileActorParent>(TheProjectileObject, GetActorLocation() + (GetActorForwardVector() * 80), FQuat::Identity.Rotator(), spawnParams);
		aProjectile->SetHomingTarget(theTarget);
		aProjectile->DamageToInflict = FMath::RandRange((int32)SpellDamageRange.X, (int32)SpellDamageRange.Y);
		IsCastingSpell = true;
		GetWorld()->GetTimerManager().SetTimer(CastSpellEndHandle, this, &ANewThirdPersonCodeCharacter::EndCastingSpell, 1, false);
	}
	CancelSpellCharging();
}

void ANewThirdPersonCodeCharacter::EndCastingSpell()
{
	IsCastingSpell = false;
}

void ANewThirdPersonCodeCharacter::StartSprinting()
{
	GetCharacterMovement()->MaxWalkSpeed = BaseWalkSpeed * RunSpeedMulti;
}

void ANewThirdPersonCodeCharacter::StopSprinting()
{
	GetCharacterMovement()->MaxWalkSpeed = BaseWalkSpeed;
}

bool ANewThirdPersonCodeCharacter::CanCastSpell() {
	if (theTarget && !IsChargingSpell && NextSpellTime < GetWorld()->GetTimeSeconds() && !IsPunching && GetCharacterMovement()->Velocity.Size() <= 0) {
		return true;
	}
	else {
		return false;
	}
}

bool ANewThirdPersonCodeCharacter::CanMeleeAttack() {
	if (theTarget && IsAttacking && !IsPunching && NextAttackTime < GetWorld()->GetTimeSeconds() && !IsChargingSpell && !IsCastingSpell && GetCharacterMovement()->Velocity.Size() <= 0) {
		return true;
	}
	else {
		return false;
	}
}

void ANewThirdPersonCodeCharacter::Use()
{
	const FVector Start = this->GetActorLocation();

	const FVector End = Start + GetControlRotation().Vector() * 256;

	FHitResult HitData(ForceInit);
	
	FHitResult Hit;

	if (GeneralStaticFunctionLibrary::Trace(this, Start, End, HitData))
	{
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Blue, HitData.ImpactPoint.ToString());
		if (HitData.GetActor())
		{
			GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Blue, HitData.GetActor()->GetName());
		}
	}
	UnsetEquipmentSlot(EEquipmentSlot::Helmet);
}

AActor* ANewThirdPersonCodeCharacter::ClickedTarget()
{
	FHitResult aHit;
	Controller->CastToPlayerController()->GetHitResultUnderCursor(ECC_Pawn, false, aHit);
	if (aHit.bBlockingHit) {
		if (aHit.Actor != NULL && aHit.GetActor()->ActorHasTag(FName("Targetable"))) {
			return aHit.GetActor();
		}
	}
	return NULL;
}

void ANewThirdPersonCodeCharacter::SelectTarget(AActor* aTarget) 
{
	AEnemyCharacterParent* aEnemy = Cast<AEnemyCharacterParent>(aTarget);
	if (aEnemy) {
		theTarget = aTarget;
		aTargetPanel->SetVisibility(ESlateVisibility::Visible);
		aTargetPanel->TargetName = aTarget->GetName();
		aTargetPanel->TargetTexture = aEnemy->EnemyTexture;
		aTargetPanel->TargetHealthPercent = aEnemy->GetHealthPercentage();
		aTargetPanel->TargetManaPercent = aEnemy->GetManaPercentage();
		aTargetPanel->TargetObject = aEnemy;
		if (PulsingDecal){
			PulsingDecal->Destroy();
		}
		FActorSpawnParameters spawnParams;
		spawnParams.Owner = this;
		spawnParams.Instigator = Instigator;
		PulsingDecal = GetWorld()->SpawnActor<APulsingDecalActor>(TheDecalObject, aTarget->GetActorLocation(), FQuat::Identity.Rotator(), spawnParams);
		PulsingDecal->InitializeDecal(aTarget->GetActorLocation(), FVector(100, 200, 200), "/Game/ThirdPerson/Textures/M_ArcaneCircle");
		PulsingDecal->InitializePulsing(20, 5);
		PulsingDecal->SetFollowTarget(aTarget, FVector(0, 0, 0));
	}
}

void ANewThirdPersonCodeCharacter::DeselectTarget()
{
	theTarget = NULL;
	aTargetPanel->TargetName = "";
	aTargetPanel->TargetTexture = GeneralStaticFunctionLibrary::LoadTextureFromPath("/Game/ThirdPerson/Textures/Icons/T_UE4Symbol");
	aTargetPanel->TargetObject = NULL;
	IsAttacking = false;
	aTargetPanel->SetVisibility(ESlateVisibility::Hidden);
	if (PulsingDecal){
		PulsingDecal->Destroy();
	}
}

void ANewThirdPersonCodeCharacter::LeftMouseReleased()
{
	if (aEquipmentPanel->IsDragging)
		aEquipmentPanel->IsDragging = false;
	if (aInventoryPanel->IsDragging)
		aInventoryPanel->IsDragging = false;
}

void ANewThirdPersonCodeCharacter::LeftMousePress()
{
	AActor* clickedActor = ClickedTarget();
	if (clickedActor) {
		//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Blue, FString("Selected Target: ") + aHit.GetActor()->GetName());
		SelectTarget(clickedActor);
	}
	else {
		//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Blue, FString("Deselected Target: ") + aTarget->GetName());
		DeselectTarget();
	}
	IsAttacking = false;
}

void ANewThirdPersonCodeCharacter::RightMousePress() {
	AActor* clickedActor = ClickedTarget();
	if (clickedActor){
		SelectTarget(clickedActor);
		IsAttacking = true;
	}
}

void ANewThirdPersonCodeCharacter::ToggleView()
{
	if (FirstPersonToggled) {
		CameraBoom->TargetArmLength = LastArmLength;
		CameraBoom->SetRelativeLocation(FVector(0, 0, 21.271957f));
		bUseControllerRotationYaw = false;
		FirstPersonToggled = false;
	}
	else {
		CameraBoom->TargetArmLength = 0;
		CameraBoom->SetRelativeLocation(FVector(29.999952f, 0, 61.271706f));
		bUseControllerRotationYaw = true;
		FirstPersonToggled = true;
	}
}

void ANewThirdPersonCodeCharacter::ScrollTowards()
{
	if (FirstPersonToggled) return;
	CameraBoom->TargetArmLength = FMath::Clamp(CameraBoom->TargetArmLength + ScrollSpeed, MinScrollAmount, MaxScrollAmount);
	LastArmLength = CameraBoom->TargetArmLength;
}

void ANewThirdPersonCodeCharacter::ScrollAway()
{
	if (FirstPersonToggled) return;
	CameraBoom->TargetArmLength = FMath::Clamp(CameraBoom->TargetArmLength - ScrollSpeed, MinScrollAmount, MaxScrollAmount);
	LastArmLength = CameraBoom->TargetArmLength;
}

bool ANewThirdPersonCodeCharacter::IsFacingTarget()
{
	if (theTarget){
		FVector normalVector = theTarget->GetActorLocation() - GetActorLocation();
		normalVector.Normalize();
		return (FVector::DotProduct(GetActorForwardVector(), normalVector ) > 0.6);
	}
	else {
		return false;
	}
}

void ANewThirdPersonCodeCharacter::InflictDamage(int32 damage, AActor* theInflictor)
{
	if (IsDead) return;
	Health = FMath::Clamp(Health - damage, 0, MaxHealth);
	if (IsHealthDepleted())
	{
		IsDead = true;
		GeneralStaticFunctionLibrary::ToggleRagdoll(GetMesh(), this);
		GetWorld()->GetTimerManager().SetTimer(RespawnHandle, this, &ANewThirdPersonCodeCharacter::CommenceRespawn, RespawnTime, false);
	}
}

void ANewThirdPersonCodeCharacter::CommenceRespawn()
{
	GetWorld()->GetTimerManager().ClearTimer(RespawnHandle);
	FVector theSpawn = GeneralStaticFunctionLibrary::GetRandomSpawnPoint(GetWorld());
	GeneralStaticFunctionLibrary::ToggleRagdoll(GetMesh(), this);
	SetActorLocation(theSpawn);
	SetActorRotation(FQuat::Identity);
	Health = MaxHealth;
	Mana = MaxMana;
	IsDead = false;
}

void ANewThirdPersonCodeCharacter::DrainMana(int32 amount)
{
	if (IsDead) return;
	Mana = FMath::Clamp(Mana - amount, 0, MaxMana);
}

bool ANewThirdPersonCodeCharacter::IsHealthDepleted()
{
	return (Health <= 0);
}

bool ANewThirdPersonCodeCharacter::HasEnoughMana(int32 amount)
{
	return (Mana >= amount);
}

void ANewThirdPersonCodeCharacter::ConsumeFood(FString itemName)
{
	if (GEngine) {
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString("Consumed " + itemName));
	}
}

void ANewThirdPersonCodeCharacter::EquipItem(FString itemName, EItemType itemType)
{
	if (GEngine) {
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString("Equipped: " + itemName));
	}
	if (GetWorld()) {
		FEquipableStruct equipStruct = GeneralStaticFunctionLibrary::EquipmentDataMap[itemName];
		UStaticMesh* theMesh = GeneralStaticFunctionLibrary::LoadStaticMeshFromPath("/Game/ThirdPerson/Meshes/Armor/VikingHelmet/VikingHelmet");
		UStaticMeshComponent* meshInst = NewObject<UStaticMeshComponent>(UStaticMeshComponent::StaticClass());
		meshInst->SetStaticMesh(theMesh);
		meshInst->SetCollisionEnabled(ECollisionEnabled::NoCollision);
		meshInst->SetWorldLocation(this->GetMesh()->GetSocketLocation(equipStruct.ItemSocketName) + equipStruct.ItemPosOffset);
		meshInst->RelativeRotation = this->GetMesh()->RelativeRotation.Add(equipStruct.ItemAngOffset.X, equipStruct.ItemAngOffset.Y, equipStruct.ItemAngOffset.Z);
		meshInst->RegisterComponentWithWorld(GetWorld());
		meshInst->SetWorldScale3D(equipStruct.ItemScale);
		meshInst->AttachTo(this->GetMesh(), equipStruct.ItemSocketName, EAttachLocation::KeepWorldPosition);
		SetEquipmentSlot(itemName, GeneralStaticFunctionLibrary::ConvertTypeEnum(itemType), meshInst);
		aEquipmentPanel->GenerateEquipmentIcons(this);
	}
}

void ANewThirdPersonCodeCharacter::UseItem(FString itemName, EItemType itemType)
{
	if (GEngine) {
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString("Used: " + itemName));
	}
}

void ANewThirdPersonCodeCharacter::SetEquipmentSlot(FString itemName, EEquipmentSlot equipSlot, UStaticMeshComponent* meshComponent)
{
	if (HasEquipmentInSlot(equipSlot)) return;
	EquipmentSlotMap.Add((uint8)equipSlot, itemName);
	EquipmentMeshMap.Add(itemName, meshComponent);
}

void ANewThirdPersonCodeCharacter::UnsetEquipmentSlot(EEquipmentSlot equipSlot)
{
	if (!HasEquipmentInSlot(equipSlot)) return;
	FString itemName = EquipmentSlotMap[(uint8)equipSlot];
	int32 itemID = GeneralStaticFunctionLibrary::FindItemIDByName(itemName);
	if (GeneralStaticFunctionLibrary::ItemIDExists(itemID))
	{
		TheInventory.Push(itemID);
		aInventoryPanel->GenerateInventoryIcons(this);
	}
	UStaticMeshComponent* theMesh = EquipmentMeshMap[itemName];
	EquipmentMeshMap.Remove(itemName);
	theMesh->DestroyComponent(true);
	EquipmentSlotMap.Remove((uint8)equipSlot);
}

bool ANewThirdPersonCodeCharacter::HasEquipmentInSlot(EEquipmentSlot equipSlot)
{
	return (EquipmentSlotMap.Contains((uint8)equipSlot));
}

FString ANewThirdPersonCodeCharacter::GetEquipmentSlot(EEquipmentSlot equipSlot)
{
	if (HasEquipmentInSlot(equipSlot))
	{
		return EquipmentSlotMap[(uint8)equipSlot];
	}
	else
	{
		return "";
	}
}

void ANewThirdPersonCodeCharacter::ToggleInventoryPanel()
{
	if (aInventoryPanel->GetVisibility() == ESlateVisibility::Hidden)
	{
		aInventoryPanel->SetVisibility(ESlateVisibility::Visible);
		aInventoryPanel->GenerateInventoryIcons(this);
	}
	else
	{
		aInventoryPanel->SetVisibility(ESlateVisibility::Hidden);
	}
}

void ANewThirdPersonCodeCharacter::ToggleEquipmentPanel()
{
	if (aEquipmentPanel->GetVisibility() == ESlateVisibility::Hidden)
	{
		aEquipmentPanel->SetVisibility(ESlateVisibility::Visible);
		aEquipmentPanel->GenerateEquipmentIcons(this);
	}
	else
	{
		aEquipmentPanel->SetVisibility(ESlateVisibility::Hidden);
	}
}